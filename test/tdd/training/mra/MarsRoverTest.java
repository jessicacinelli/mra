package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {


	//Tests user story 1
	@Test
	public void test() throws MarsRoverException{
		int planetX = 10;
		int planetY=10;
		List<String> obstacles= new ArrayList<String> ();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		MarsRover mr= new MarsRover(planetX, planetY, obstacles);
		assertEquals(planetX, mr.getPlanetX());
		assertEquals(planetY, mr.getPlanetY());
		assertEquals(true, mr.equals(obstacles));
	}
	
	//Tests user story 2
	@Test
	public void testLanding() throws MarsRoverException{
		int planetX = 10;
		int planetY=10;
		List<String> obstacles= new ArrayList<String> ();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		MarsRover mr= new MarsRover(planetX, planetY, obstacles);
		mr.executeCommand("");
		String status = "(0,0,N)";
		assertEquals(planetX, mr.getPlanetX());
		assertEquals(planetY, mr.getPlanetY());
		assertEquals(true, mr.equals(obstacles));
		assertEquals(status, mr.getStatus());
	}

	//Tests user story 3.1
	@Test
	public void testTurningR() throws MarsRoverException{
		int planetX = 10;
		int planetY=10;
		List<String> obstacles= new ArrayList<String> ();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		MarsRover mr= new MarsRover(planetX, planetY, obstacles);
		mr.executeCommand("");
		mr.executeCommand("r");
		String status = "(0,0,E)";
		assertEquals(planetX, mr.getPlanetX());
		assertEquals(planetY, mr.getPlanetY());
		assertEquals(true, mr.equals(obstacles));
		assertEquals(status, mr.getStatus());
	}
	
	
	//Tests user story 3.2
	@Test
	public void testTurningL() throws MarsRoverException{
		int planetX = 10;
		int planetY=10;
		List<String> obstacles= new ArrayList<String> ();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		MarsRover mr= new MarsRover(planetX, planetY, obstacles);
		mr.executeCommand("");
		mr.executeCommand("l");
		String status = "(0,0,W)";
		assertEquals(planetX, mr.getPlanetX());
		assertEquals(planetY, mr.getPlanetY());
		assertEquals(true, mr.equals(obstacles));
		assertEquals(status, mr.getStatus());
	}
	
	//Tests user story 4
	@Test
	public void testMovingF() throws MarsRoverException{
		int planetX = 10;
		int planetY=10;
		List<String> obstacles= new ArrayList<String> ();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		MarsRover mr= new MarsRover(planetX, planetY, obstacles);
		
		String ns="(7,6,N)";
		mr.setStatus(ns);
		assertEquals(ns, mr.getStatus());
		mr.executeCommand("f");
		String status = "(7,7,N)";
		assertEquals(planetX, mr.getPlanetX());
		assertEquals(planetY, mr.getPlanetY());
		assertEquals(true, mr.equals(obstacles));
		assertEquals(status, mr.getStatus());
		
	}
//Tests user story 5	
	@Test
	public void testMovingB() throws MarsRoverException{
		int planetX = 10;
		int planetY=10;
		List<String> obstacles= new ArrayList<String> ();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		MarsRover mr= new MarsRover(planetX, planetY, obstacles);
		String ns="(5,8,E)";
		mr.setStatus(ns);
		assertEquals(ns, mr.getStatus());
		mr.executeCommand("b");
		String status = "(4,8,E)";
		assertEquals(planetX, mr.getPlanetX());
		assertEquals(planetY, mr.getPlanetY());
		assertEquals(true, mr.equals(obstacles));
		assertEquals(status, mr.getStatus());
		
	}
	
	//Tests user story 6	
		@Test
		public void testMovingCombined() throws MarsRoverException{
			int planetX = 10;
			int planetY=10;
			List<String> obstacles= new ArrayList<String> ();
			obstacles.add("(4,7)");
			obstacles.add("(2,3)");
			MarsRover mr= new MarsRover(planetX, planetY, obstacles);
			mr.setStatus("(0,0,N)");
			mr.executeCommand("ffrff");
			String status = "(2,2,E)";
			assertEquals(planetX, mr.getPlanetX());
			assertEquals(planetY, mr.getPlanetY());
			assertEquals(true, mr.equals(obstacles));
			assertEquals(status, mr.getStatus());
			
		}
		
		//Tests user story 7	
		@Test
		public void testWrapping() throws MarsRoverException{
			int planetX = 10;
			int planetY=10;
			List<String> obstacles= new ArrayList<String> ();
			obstacles.add("(4,7)");
			obstacles.add("(2,3)");
			MarsRover mr= new MarsRover(planetX, planetY, obstacles);
			mr.setStatus("(0,0,N)");
			mr.executeCommand("b");
			String status = "(0,9,N)";
			assertEquals(planetX, mr.getPlanetX());
			assertEquals(planetY, mr.getPlanetY());
			assertEquals(true, mr.equals(obstacles));
			assertEquals(status, mr.getStatus());
			
		}
		
		//Tests user story 8	
				@Test
				public void testSingleObstacle() throws MarsRoverException{
					int planetX = 10;
					int planetY=10;
					List<String> obstacles= new ArrayList<String> ();
					obstacles.add("(2,2)");
					MarsRover mr= new MarsRover(planetX, planetY, obstacles);
					mr.setStatus("(0,0,N)");
					mr.executeCommand("ffrfff");
					String status = "(1,2,E)(2,2)";
					assertEquals(planetX, mr.getPlanetX());
					assertEquals(planetY, mr.getPlanetY());
					assertEquals(true, mr.equals(obstacles));
					assertEquals(status, mr.getStatus());
					
				}
				
				//Tests user story 9	
				@Test
				public void testMultipleObstacles() throws MarsRoverException{
					int planetX = 10;
					int planetY=10;
					List<String> obstacles= new ArrayList<String> ();
					obstacles.add("(2,2)");
					obstacles.add("(2,1)");
					
					MarsRover mr= new MarsRover(planetX, planetY, obstacles);
					mr.setStatus("(0,0,N)");
					mr.executeCommand("ffrfffrflf");
					String status = "(1,1,E)(2,2)(2,1)";
					assertEquals(planetX, mr.getPlanetX());
					assertEquals(planetY, mr.getPlanetY());
					assertEquals(true, mr.equals(obstacles));
					assertEquals(status, mr.getStatus());
					
				}
				
				//Tests user story 10
				@Test
				public void testWrappingAndObstacles() throws MarsRoverException{
					int planetX = 10;
					int planetY=10;
					List<String> obstacles= new ArrayList<String> ();
					obstacles.add("(0,9)");
					
					
					MarsRover mr= new MarsRover(planetX, planetY, obstacles);
					mr.setStatus("(0,0,N)");
					mr.executeCommand("b");
					String status = "(0,0,N)(0,9)";
					assertEquals(planetX, mr.getPlanetX());
					assertEquals(planetY, mr.getPlanetY());
					assertEquals(true, mr.equals(obstacles));
					assertEquals(status, mr.getStatus());
					
				}
				
				//Tests user story 11
				@Test
				public void testAroundThePlanet() throws MarsRoverException{
					int planetX = 6;
					int planetY=6;
					List<String> obstacles= new ArrayList<String> ();
					obstacles.add("(2,2)");
					obstacles.add("(0,5)");
					obstacles.add("(5,0)");
					
					MarsRover mr= new MarsRover(planetX, planetY, obstacles);
					mr.setStatus("(0,0,N)");
					mr.executeCommand("ffrfffrbbblllfrfrbbl");
					String status = "(0,0,N)(2,2)(0,5)(5,0)";
					assertEquals(planetX, mr.getPlanetX());
					assertEquals(planetY, mr.getPlanetY());
					assertEquals(true, mr.equals(obstacles));
					assertEquals(status, mr.getStatus());
					
				}
}
