package tdd.training.mra;


import java.util.ArrayList;

import java.util.List;

public class MarsRover {
	private int planetX;
	private int planetY;
	private String status ="(0,0,N)";
	private List<String> planetObstacles =new ArrayList<String>();
	private boolean containsObstacles = false;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX=planetX;
		this.planetY=planetY;
		this.planetObstacles= planetObstacles;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(planetObstacles.contains("(" + x + "," + y +")"))
			containsObstacles = true;
		return containsObstacles;
	}



	public int getPlanetX() {
		return planetX;
	}



	public int getPlanetY() {
		return planetY;
	}


	public List<String> getPlanetObstacles() {
		return planetObstacles;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status)    {
		this.status=status;
	}

	public boolean equals(List<String> obstacles) {
		boolean equals = false;
		if(planetObstacles.containsAll(obstacles))
			equals= true;
		return equals;
	}

	public void setContainsObstacles(boolean containsObstacles) {
		this.containsObstacles = containsObstacles;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		char command;

		String x;
		String y;
		String d;
		String obstacles="";
		String position = "";

		for(int i=0; i<commandString.length(); ++i) {
			x = status.substring(1,2);
			y= status.substring(3,4);
			d= status.substring(5,6);
			int movex;
			int movey;
			command=commandString.charAt(i);
			String sequence="NESW";
			if(command=='r') {


				if(sequence.indexOf(d)==sequence.length()-1)
					d=sequence.charAt(0)+"";
				else
					d=sequence.charAt(sequence.indexOf(d)+1)+"";
				status= "("+ x + "," + y + "," + d + ")";
			}
			else if(command=='l') {	

				if(sequence.indexOf(d)==0)
					d=sequence.charAt(sequence.length()-1)+"";
				else
					d=sequence.charAt(sequence.indexOf(d)-1)+"";
				status= "("+ x + "," + y + "," + d + ")";
			}

			else {
				String cmd=command+d;
				switch(cmd) {
				case "fN": case "bS":
					movey=Integer.parseInt(y);
					if (movey==planetY-1)
						y= "" +0;
					else  y="" + ++movey;
					break;
				case "fS": case "bN": 
					movey=Integer.parseInt(y);
					if (movey==0)
						y= "" +(planetY-1);
					else  y="" + --movey;
					break;

				case "fE": case "bW":
					movex=Integer.parseInt(x);
					if (movex==planetX-1)
						x=0 +"";
					else x=""+ ++movex ;
					break;
				case "bE": case "fW":
					movex=Integer.parseInt(x);
					if (movex==0)
						x= ""+(planetX-1);
					else x= "" + --movex;
					break;
				}

			}

			position = "(" + x + "," + y +")";
			if(Obstacle(position) ) {
				if(!obstacles.contains(position))
					obstacles=obstacles.concat(position);
			}
			else
				status="(" + x + "," +  y + "," + d + ")";

		}
		
		status=status.concat(obstacles);

		return status;
	}

	public boolean Obstacle(String position) {
		boolean found = false;
		if (planetObstacles.contains(position))
			found = true;
		return found;
	}

}
